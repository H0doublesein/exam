﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.People
{
    public interface IPersonFinder
    {
        bool IsThereUserWithThisId(int Id);
    }
}
