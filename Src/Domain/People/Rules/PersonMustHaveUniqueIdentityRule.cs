﻿using Domain.Core.Interfaces;
using Domain.People;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Users.Rules
{
    public class PersonMustHaveUniqueIdentityRule : IBusinessRule
    {
        public string Message => "The Id Has Already Exist";
        public int  Id { get; private set; }
        IPersonFinder personFinder;

        public PersonMustHaveUniqueIdentityRule(int Id, IPersonFinder personFinder)
        {
            this.Id = Id;
            this.personFinder = personFinder;
        }
        public bool IsBroken()
        {
            return personFinder.IsThereUserWithThisId(Id);
        }
    }
}
