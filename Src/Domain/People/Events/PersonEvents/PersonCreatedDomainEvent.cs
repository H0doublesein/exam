﻿using Domain.Core;
using Domain.People;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Users.Events.PersonEvents
{
   public class PersonCreatedDomainEvent:DomainEvent
    {
        public PersonCreatedDomainEvent(Person person)
        {
            this.person = person;
        }
        public Person person { get; }
    }
}
