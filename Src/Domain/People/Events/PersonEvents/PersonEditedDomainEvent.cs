﻿using Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.People.Events.PersonEvents
{
   public class PersonEditedDomainEvent:DomainEvent
    {
        public PersonEditedDomainEvent(Person person)
        {
            this.person = person;
        }
        public Person person { get; }
    }
}
