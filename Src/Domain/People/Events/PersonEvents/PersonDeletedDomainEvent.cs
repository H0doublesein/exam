﻿using Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.People.Events.PersonEvents
{
   public class PersonDeletedDomainEvent : DomainEvent
    {
        public PersonDeletedDomainEvent(Person person)
        {
            this.person = person;
        }
        public Person person { get; }
    }
}
