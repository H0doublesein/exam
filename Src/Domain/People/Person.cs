﻿using Domain.Core;
using Domain.Core.Interfaces;
using Domain.People.Events.PersonEvents;
using Domain.Users.Events;
using Domain.Users.Events.PersonEvents;
using Domain.Users.Rules;
using System;
using System.Runtime.CompilerServices;

namespace Domain.People
{
    public class Person : Entity, IAggregateRoot
    {
        #region Props
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDay { get; set; }
        public string Config { get; set; }
        #endregion
        public Person() { } // just for Auto Mapper

        private Person(int Id,string Name , DateTime BirthDay , string Config, IPersonFinder personFinder) {
            this.Id = Id;
            this.Name = Name;
            this.BirthDay = BirthDay;
            this.Config = Config; 
            this.CheckRule(new PersonMustHaveUniqueIdentityRule(Id,personFinder));
            this.AddDomainEvent(new PersonCreatedDomainEvent(this));
        }
        public static Person Create(int Id, string Name, DateTime BirthDay, string Config,IPersonFinder personFinder)
        {
            return new Person (Id,Name,BirthDay,Config,personFinder);
        }
        public int Edit(Person person)
        { 
            AddDomainEvent(new PersonEditedDomainEvent(person));
            return person.Id;
        }
        public void Delete(Person person)
        {
            AddDomainEvent(new PersonDeletedDomainEvent(person));
        }
    }
}
