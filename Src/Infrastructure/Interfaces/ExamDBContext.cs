﻿using Domain.People;
using Infrastructure.Configurations;
using Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Interfaces
{
    public class ExamDBContext : DbContext
    {
        public virtual DbSet<PersonEntity> People { get; set; }
       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // It is not safe to hard code connection string in the code, I hard coded it just for this exam

                optionsBuilder.UseSqlServer(@"Server=.;Database=ExamDB;Trusted_Connection=True;");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Ignore<Config>();
            modelBuilder.ApplyConfiguration(new PersonEntityConfiguration());
        }
    }
}
