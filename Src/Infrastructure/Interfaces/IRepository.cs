﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces
{  
        public interface IRepository<T> where T : class
        {
            IEnumerable<T> Get();
            IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
            void Add(T entity);
            void Delete(Expression<Func<T, bool>> identity);
            void Update(T entity);
        } 
}
