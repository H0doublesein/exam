﻿using Common;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class UnitOfWork<T> : IUnitOfWork<T> where T : class
    {
        public ExamDBContext Context { get; }

        public IRepository<T> _repo { get; private set; }

        public IRepository<T> Repository => _repo;

        public UnitOfWork(ExamDBContext context)
        {
            Context = context;
            _repo = new Repository<T>(context);
        }
        public void Commit()
        {
            Context.SaveChanges();
        } 

        public void Dispose()
        {
            Context.Dispose();
        }

        public Task CommitAsync()
        {
          return  Context.SaveChangesAsync();
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }
    }
}
