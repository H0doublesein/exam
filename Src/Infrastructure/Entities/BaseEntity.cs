﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Entities
{
   public class BaseEntity
    {
        public BaseEntity()
        {
            EditedAt = null;
            CreatedAt = DateTime.Now;
        }
        public int ID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? EditedAt { get; set; }
    }
}
