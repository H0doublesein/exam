﻿using Domain.People;
using System;

namespace Infrastructure.Entities
{
    public class PersonEntity:BaseEntity
    {  
        public string Name { get; set; }
        public DateTime BirthDay { get; set; } 
        public string Config { get; set; }
    }
}
