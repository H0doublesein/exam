﻿using Domain.People;
using Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Configurations
{
    public class PersonEntityConfiguration : IEntityTypeConfiguration<PersonEntity>
    {
        public void Configure(EntityTypeBuilder<PersonEntity> builder)
        {
            builder.ToTable("People");
            builder.HasKey(e => e.ID); // it's not necessary. convetion over configuration prenciple :)
            builder.Property(e => e.ID).UseSqlServerIdentityColumn();
            builder.Property(e => e.Name)
                    .IsRequired(true)
                    .HasMaxLength(50)
                    .IsUnicode(true);
        }
    }
}
