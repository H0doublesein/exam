﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
   public class TransactionResult
    {
        public string Message { get; set; } 
        public object Value { get; set; } 
        public TransactionStatus Status { get; set; }
    }
}
