﻿using System;

namespace Common
{
    public enum Gender
    {
        Male =1 ,
        Female = 2
    }
    public enum TransactionStatus
    {
        Success=0,
        Fail=1
    }
    public enum CommandResultStatus
    {
        Pending=0,
        Success=1,
        Fail=2
    }
    public enum QueryResultStatus
    {
        Pending=0,
        Success=1,
        Fail=2
    }
}
