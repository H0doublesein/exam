﻿using AutoMapper;
using Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Configuration.Mapper
{
   public class AutoMapping:Profile
    {
        public AutoMapping()
        {
            CreateMap<Domain.People.Person, Infrastructure.Entities.PersonEntity>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.ID, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.BirthDay, opt => opt.MapFrom(s => s.BirthDay))
                .ForMember(d => d.Config, opt => opt.MapFrom(s => s.Config));

            CreateMap<Infrastructure.Entities.PersonEntity, Domain.People.Person>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.ID))
                .ForMember(d => d.BirthDay, opt => opt.MapFrom(s => s.BirthDay)) 
                .ForMember(d => d.Config, opt => opt.MapFrom(s => s.Config));
        } 
    }
}
