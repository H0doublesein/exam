﻿using Common;
using System;
using System.Collections.Generic;
using System.Text; 

namespace Application.Configuration.Queries
{
   public class QueryResult
    {
        public QueryResultStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
