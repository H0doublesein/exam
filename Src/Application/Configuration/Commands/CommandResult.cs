﻿using Common;
using System;
using System.Collections.Generic;
using System.Text; 

namespace Application.Configuration.Commands
{
   public class CommandResult
    {
        public CommandResultStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
