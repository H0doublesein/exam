﻿using Application.Configuration.Commands;
using AutoMapper;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using MediatR; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Users.Commands.SignUpCommands
{
    internal class DeleteCommandHandler: ICommandHandler<DeleteCommand, CommandResult>
    {
        private readonly IUnitOfWork<PersonEntity> _uow;
        private readonly IRepository<PersonEntity> _repo;
        private readonly IMapper mapper;
        public DeleteCommandHandler(IUnitOfWork<PersonEntity> uow, IRepository<PersonEntity> repo, IMapper mapper)
        {
            this._uow= uow;
            this._repo = repo;
            this.mapper = mapper;
        }

        public async Task<CommandResult> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            try
            { 
                _uow.Repository.Delete(p=>p.ID==request.PersonId);
                await _uow.CommitAsync();
                return new CommandResult() { Data = request.Id, Status = Common.CommandResultStatus.Success };
            }
            catch(Exception exp)
            {
                return new CommandResult() { Message = exp.Message, Status = Common.CommandResultStatus.Fail };
            }
        }

       
    }
}
