﻿using Application.Configuration.Commands;
using Domain.Users; 
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Users.Commands.SignUpCommands
{
    public class DeleteCommand:CommandBase<CommandResult>
    { 
        public DeleteCommand(int UserId)
        {
            this.PersonId = UserId; 
        }
        public int PersonId { get; private set; } 
    }
}
