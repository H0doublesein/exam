﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Users.Commands.SignUpCommands
{
   public class DeleteCommandValidator : AbstractValidator<DeleteCommand>
    {
        public DeleteCommandValidator()
        {
            this.RuleFor(x => x.PersonId).NotEmpty().WithMessage("Person Id is mandatory !");
        }
    }
}
