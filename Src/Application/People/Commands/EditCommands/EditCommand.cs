﻿using Application.Configuration.Commands;
using Domain.People;
using Domain.Users; 
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.People.Commands.EditCommands
{
    public class EditCommand : CommandBase<CommandResult>
    { 
        public EditCommand(Person person)
        {
            this.Person = person;
        }
        public Person Person { get; private set; } 
    }
}
