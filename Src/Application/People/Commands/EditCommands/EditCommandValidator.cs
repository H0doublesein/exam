﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.People.Commands.EditCommands
{
   public class EditCommandValidator : AbstractValidator<EditCommand>
    {
        public EditCommandValidator()
        {
            this.RuleFor(x => x.Id).NotEmpty().WithMessage("Person Id is mandatory !");
        }
    }
}
