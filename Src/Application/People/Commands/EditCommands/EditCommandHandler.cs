﻿using Application.Configuration.Commands;
using AutoMapper;
using Domain.People;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using MediatR; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.People.Commands.EditCommands
{
    internal class EditCommandHandler : ICommandHandler<EditCommand, CommandResult>
    {
        private readonly IUnitOfWork<PersonEntity> _uow;
        private readonly IRepository<PersonEntity> _repo;
        private readonly IPersonFinder _personFinder;
        private readonly IMapper mapper;
        public EditCommandHandler(IRepository<PersonEntity> repo, IUnitOfWork<PersonEntity> uow,
            IPersonFinder personFinder, IMapper mapper)
        {
            this._repo = repo;
            this._personFinder = personFinder;
            this._uow=uow; 
            this.mapper = mapper;
        }

        public async Task<CommandResult> Handle(EditCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var personEntity = mapper.Map<PersonEntity>(request.Person);
                // _repo.Update(personEntity);
                 _uow.Repository.Update(personEntity);
                 _uow.Commit();
                return new CommandResult() { Data = personEntity.ID, Status = Common.CommandResultStatus.Success };
            }
            catch(Exception exp)
            {

                return new CommandResult() { Status = Common.CommandResultStatus.Fail , Message =exp.Message };
            }
        }

       
    }
}
