﻿using Application.Configuration.Commands;
using Domain.People;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Users.Commands.SignInCommands
{
    public class CreateCommand : CommandBase<CommandResult>
    { 
        public CreateCommand(string Name , string Config , DateTime BirthDay)
        { 
            this.Name = Name;
            this.Config = Config;
            this.BirthDay = BirthDay;
        } 
        public string Name { get; private set; }
        public string Config { get; private set; }
        public DateTime BirthDay { get; private set; }
    }
}
