﻿using Application.Configuration.Commands;
using AutoMapper;
using Domain.Users; 
using Infrastructure.Entities; 
using Infrastructure.Interfaces; 
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Users.Commands.SignInCommands
{
    internal class CreateCommandHandler : ICommandHandler<CreateCommand, CommandResult>
    {
        private readonly IUnitOfWork<PersonEntity> _uow;
        private readonly IRepository<PersonEntity> _repo;
        public CreateCommandHandler(IUnitOfWork<PersonEntity> uow, IRepository<PersonEntity> _repo)
        {
            this._uow = uow;
            this._repo = _repo;
        }

        public async Task<CommandResult> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            try
            { 
                _uow.Repository.Add(new PersonEntity()
                {
                    BirthDay = request.BirthDay,
                    Config = request.Config,
                    Name = request.Name,
                    CreatedAt = DateTime.Now
                });
                await _uow.CommitAsync();
                return new CommandResult()
                {
                    Status = Common.CommandResultStatus.Success
                };
            }
            catch(Exception exp) { 
                return new CommandResult() { Status = Common.CommandResultStatus.Fail, Message = exp.Message};
            }
        }       
    }
}
