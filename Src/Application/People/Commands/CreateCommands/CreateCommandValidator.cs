﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Users.Commands.SignInCommands
{
   public class CreateCommandValidator : AbstractValidator<CreateCommand>
    {
        public CreateCommandValidator()
        {
            this.RuleFor(x => x.Name).NotEmpty().WithMessage("Person Name is Mandatory !");
        }
    }
}
