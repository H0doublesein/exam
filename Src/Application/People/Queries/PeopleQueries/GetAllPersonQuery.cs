﻿using Application.Configuration.Queries;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Application.People.Queries.PeopleQueries
{
   public class GetAllPersonQuery: QueryBase<QueryResult>
    {
        public GetAllPersonQuery(string Name, DateTime BirthDate)
        {
            this.Name = Name;
            this.BirthDate = BirthDate;
        }
        public string  Name { get; private set; }
        public DateTime BirthDate { get; private set; }
    }
}
