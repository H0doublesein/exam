﻿using Application.People.Queries.PeopleQueries;
using FluentValidation; 
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Users.Queries.UserQueries
{
   public class GetPersonQueryValidator:AbstractValidator<GetPersonQuery>
    {
        public GetPersonQueryValidator()
        {
            this.RuleFor(x => x.Id).NotEqual(null).WithMessage("Person Id is Required !"); 
        }
    }
}
