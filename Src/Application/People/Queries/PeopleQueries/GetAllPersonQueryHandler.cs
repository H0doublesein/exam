﻿using Application.Configuration.Queries;
using AutoMapper;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.People.Queries.PeopleQueries
{
   public class GetAllPersonQueryHandler:IQueryHandler<GetAllPersonQuery, QueryResult>
    {
        IUnitOfWork<PersonEntity> _uow;
        private readonly IRepository<PersonEntity> _repo;
        IMapper _mapper;
        public GetAllPersonQueryHandler(IUnitOfWork<PersonEntity> uow, IMapper mapper, IRepository<PersonEntity> repo)
        {
            this._repo = repo;
            this._uow = uow;
            this._mapper = mapper;
        }
        public async Task<QueryResult> Handle(GetAllPersonQuery request, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<PersonEntity> people;
                if (string.IsNullOrEmpty(request.Name) && request.BirthDate == DateTime.MinValue)
                {
                    people = _uow.Repository.Get();
                }
                else
                {
                    people = _uow.Repository.Get(p => p.Name.Equals(request.Name) || p.BirthDay.Equals(request.BirthDate));
                }
                var DomainUsers = _mapper.Map<List<Domain.People.Person>>(people);
                return new QueryResult()
                {
                    Data = DomainUsers,
                    Status = Common.QueryResultStatus.Success,
                    Message = DomainUsers == null || DomainUsers.Count == 0 ? "Person Not Found !" : ""
                };
            }
            catch(Exception exp)
            {
                return new QueryResult()
                {
                    Status = Common.QueryResultStatus.Fail,
                    Message = exp.Message
                };
            }
        }
    }
}
