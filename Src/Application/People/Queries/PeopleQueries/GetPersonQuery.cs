﻿using Application.Configuration.Queries;
using Common;
using Domain; 
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.People.Queries.PeopleQueries
{
   public class GetPersonQuery:QueryBase<QueryResult>
    {
        public GetPersonQuery(int Id)
        {
            this.Id = Id; 
        }
        public int Id { get; private set; }  
    }
}
