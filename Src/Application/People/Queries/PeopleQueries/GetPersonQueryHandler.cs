﻿using Application.Configuration.Queries;
using Application.People.Queries.PeopleQueries;
using AutoMapper;
using Domain.Users;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.People.Queries.PeopleQueries
{
    public class GetPersonQueryHandler : IQueryHandler<GetPersonQuery, QueryResult>
    {
        IUnitOfWork<PersonEntity> _uow;
        IRepository<PersonEntity> _repo;
        IMapper _mapper;
        public GetPersonQueryHandler(IUnitOfWork<PersonEntity> uow, IMapper mapper, IRepository<PersonEntity> repo)
        {
            this._uow = uow;
            this._mapper = mapper;
            this._repo = repo;
        }
        public async Task<QueryResult> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            var person = _uow.Repository.Get(p=>p.ID == request.Id).FirstOrDefault();
            var DomainUser = _mapper.Map<Domain.People.Person>(person);
            return new QueryResult()
            {
                Data = DomainUser,
                Status = DomainUser == null ? Common.QueryResultStatus.Fail : Common.QueryResultStatus.Success,
                Message = DomainUser == null ? "User Not Found !" : ""
            };
        }
    }
}
