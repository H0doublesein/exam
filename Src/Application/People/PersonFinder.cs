﻿using Domain.People;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.People
{
   public class PersonFinder: IPersonFinder
    { 
        private readonly IRepository<PersonEntity> _repo;

        public PersonFinder(IRepository<PersonEntity> repo)
        { 
          _repo=repo;
        }

        public bool IsThereUserWithThisId(int Id)
        {
            return _repo.Get(p=>p.ID==Id) != null;
        }
    }
}
