﻿using Domain.Core.Interfaces;
using Domain.Users.Events;
using Domain.Users.Events.PersonEvents;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Users.Events
{
    public class CreateNewPersonEventHandler : INotificationHandler<PersonCreatedDomainEvent>
    {  
        public Task Handle(PersonCreatedDomainEvent notification, CancellationToken cancellationToken)
        {
            // Handle this notification with SMS, Email, ...
            return null;
        }
    }
}
