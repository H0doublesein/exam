using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MediatR;
using Domain.People;
using Application.People;
using Infrastructure.Interfaces;
using Infrastructure;
using Infrastructure.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<ExamDBContext>();

            // Auto Mapper Configurations
            services.AddAutoMapper(typeof(Startup), typeof(Infrastructure.Entities.PersonEntity), typeof(Domain.People.Person));
            services.AddAutoMapper(typeof(Startup), typeof(Domain.People.Person), typeof(Infrastructure.Entities.PersonEntity));
            services.AddAutoMapper(typeof(Startup), typeof(API.Models.UpdatePersonModel), typeof(Domain.People.Person));
            services.AddAutoMapper(typeof(Startup),  typeof(Domain.People.Person), typeof(API.Models.UpdatePersonModel));

            services.AddTransient<ExamDBContext>();
            services.AddTransient(typeof(IRepository<>),typeof(Repository<>));
            services.AddTransient(typeof(IUnitOfWork<>),typeof(UnitOfWork<>));            
            services.AddTransient<IPersonFinder, PersonFinder>();
            services.AddMediatR(typeof(Startup), typeof(Application.Configuration.Commands.CommandBase));
            services.AddMediatR(typeof(Startup), typeof(Application.Configuration.Commands.CommandBase<>));

            // Register the Swagger generator
            services.AddSwaggerGen();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            UpdateDatabase(app);
            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ExamDBContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
