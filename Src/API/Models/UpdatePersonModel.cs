﻿using Domain.People;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class UpdatePersonModel
    {
        public int Id { get; set; }
        public DateTime BirthDay { get; set; }
        public string Name { get; set; }
        public object Config { get; set; }
    }
}
