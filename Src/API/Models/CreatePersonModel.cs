﻿using Domain.People;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class CreatePersonModel
    {
        public string Name { get; set; }
        public DateTime BirthDay { get; set; }
        public object Config { get; set; }
    }
   
     
}
