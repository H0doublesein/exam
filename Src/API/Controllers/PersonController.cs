﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.Models;
using Application.People.Commands.EditCommands;
using Application.People.Queries.PeopleQueries;
using Application.Users.Commands.SignInCommands;
using Application.Users.Commands.SignUpCommands;
using AutoMapper;
using Domain.People;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("/api/Person")]
    public class PersonController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly ILogger<PersonController> _logger;
        public PersonController(ILogger<PersonController> logger, IMediator mediator, IMapper mapper)
        {
            _logger = logger;
            _mediator = mediator;
            _mapper = mapper;
        }
        /// <summary>
        /// Get People With Given Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Get/{id}")]
        public async Task<IActionResult> GetById(int id , CancellationToken token)
        {
           var result =await _mediator.Send(new GetPersonQuery(id),token);
            return Ok(result);
        }
        /// <summary>
        /// Return all people with given name or birth day
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll([FromQuery]GetAllPeopleModel model ,CancellationToken token)
        {
            var result = await _mediator.Send(new GetAllPersonQuery(model.Name,model.BirthDay), token);
            return Ok(result);
        }
        /// <summary>
        /// Create a person with given informations
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody]CreatePersonModel model, CancellationToken token)
        { 
            var result = await _mediator.Send(new CreateCommand(model.Name, model.Config.ToString().Trim(), model.BirthDay), token);
            return Ok(result); 
        }
        /// <summary>
        /// Delete Person With Given Id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete("Delete")]
        public async Task<IActionResult> DeleteById(DeletePersonModel model, CancellationToken token)
        {
            var result = await _mediator.Send(new DeleteCommand(model.Id), token);
            return Ok(result);
        }
        /// <summary>
        /// Update person with given id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdatePersonModel model, CancellationToken token)
        {           
            var mappedPerson = _mapper.Map<Person>(model);
            var result = await _mediator.Send(new EditCommand(mappedPerson), token);
            return Ok(result);
        }
    }
}