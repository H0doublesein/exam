﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class IgnoreMemberAttribute : Attribute
    {
    }
}
