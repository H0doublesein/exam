﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core.Interfaces
{
    public interface IDomainEvent : INotification
    {
        DateTime OccurredOn { get; }
    }
}
