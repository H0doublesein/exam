﻿using Domain.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    public class DomainEvent:IDomainEvent
    {
        public DateTime OccurredOn { get; }

        public DomainEvent()
        {
            this.OccurredOn = DateTime.UtcNow;
        }
    }
}
